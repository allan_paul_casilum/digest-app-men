let faker = require('faker');
let TrelloCard = require('../models/trello.model.js');
let _this = this;

//retrieve all notes
exports.index = function(req, res) {
	TrelloCard.find( (err, trello_cards) => {
		if(err) {
			console.log(err);
			res.status(500).send({message: 'Some error occured while retrieving trello cards.'});
		} else {
			res.send(trello_cards);
		}
	});
};

//show single
exports.show = function(req, res, _findById) {
	/*let _movie_id = req.params.movieId;
	
	Movie.findById(_movie_id, (err, movie) => {
		if(err) {
			console.log(err);
			if(err.kind === 'ObjectId') {
				return res.status(400).send({message: 'Movie not found with id ' + _movie_id});
			}
			return res.status(500).send({message: 'Error retrieving movie with id ' + _movie_id});
		}
		
		if(!movie) {
			return res.status(404).send({message: 'Movie not found with id ' + _movie_id});
		}
		
		res.send(movie);
	});*/
};

//create
exports.create = function(req, res) {
	//create and save new movie
	//console.log(req.body);
	if(!req.body.title) {
		return res.status(400).send({message: 'Title cannot be empty'});
	}
	
	if(!req.body.content) {
		return res.status(400).send({message: 'Content cannot be empty'});
	}
	
	var note = new Notes({
		title: req.body.title,
		content: req.body.content
	});
	
	note.save( (err, data) => {
		if(err) {
			console.log(err);
			res.status(500).send({message: 'some error occured white creating the notes'});
		} else {
			res.send(data);
		}
	});
}

//update
exports.update = function(req, res) {
	/*let _movie_id = req.params.movieId;
	
	Movie.findById(_movie_id, (err, movie) => {
		if(err) {
			console.log(err);
			if(err.kind === 'ObjectId') {
				return res.status(400).send({message: 'Movie not found with id ' + _movie_id});
			}
			return res.status(500).send({message: 'Error retrieving movie with id ' + _movie_id});
		}
		
		if(!movie) {
			return res.status(404).send({message: 'Movie not found with id ' + _movie_id});
		}
		
		movie.title = req.body.title;
		
		movie.save((err, data) => {
			if(err) {
				res.status(500).send({message: 'Could not update movie with id ' + _movie_id});
			} else {
				res.send(data);
			}
		});
		
	});*/
}

//delete
exports.delete = function(req, res) {
	/*let _movie_id = req.params.movieId;
	
	Movie.findByIdAndRemove(_movie_id, (err, movie) => {
		if(err) {
			console.log(err);
            if(err.kind === 'ObjectId') {
                return res.status(404).send({message: "Movie not found with id " + _movie_id});                
            }
            return res.status(500).send({message: "Could not delete movie with id " + _movie_id});
		}
		
		if(!movie) {
			return res.status(404).send({message: 'Movie not found with id ' + _movie_id});
		}
		
		res.send({message: "Movie deleted successfully!"})
		
	});*/
}

//generate fake data
exports.createFakeData = (req, res) => {
	for (var i = 0; i < 10; i++) {
		var trello_cards = new TrelloCard({
			title: faker.lorem.words(),
			board: faker.lorem.text(),
			list: faker.lorem.words()
		});
		trello_cards.save( (err, data) => {
			if(err) {
				console.log(err);
				//res.status(500).send({message: 'some error occured white creating the trello cards'});
			} else {
				//res.send(data);
				console.log(data);
			}
		});
	}
	res.redirect('/');
}