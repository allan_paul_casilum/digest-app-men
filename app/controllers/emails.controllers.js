let faker = require('faker');
let Emails = require('../models/email.model.js');
let _this = this;

//retrieve all emails
exports.index = function(req, res) {
	Emails.find( (err, emails) => {
		if(err) {
			console.log(err);
			res.status(500).send({message: 'Some error occured while retrieving emails.'});
		} else {
			res.send(emails);
		}
	});
};

//show single
exports.show = function(req, res, _findById) {
	/*let _movie_id = req.params.movieId;
	
	Movie.findById(_movie_id, (err, movie) => {
		if(err) {
			console.log(err);
			if(err.kind === 'ObjectId') {
				return res.status(400).send({message: 'Movie not found with id ' + _movie_id});
			}
			return res.status(500).send({message: 'Error retrieving movie with id ' + _movie_id});
		}
		
		if(!movie) {
			return res.status(404).send({message: 'Movie not found with id ' + _movie_id});
		}
		
		res.send(movie);
	});*/
};

exports.apiCreate = function(req, res) {
	var email = new Emails({
		sender: req.body.sender || faker.internet.email(),
		subject: req.body.subject || faker.lorem.words(),
		content: req.body.content || faker.lorem.text()
	});
	
	email.save( (err, data) => {
		if(err) {
			console.log(err);
			res.status(500).send({message: 'some error occured white creating the email'});
		} else {
			res.send(data);
		}
	});
}

//create
exports.create = function(req, res) {
	//create and save new emails
	//console.log(req.body);
	if(!req.body.sender) {
		return res.status(400).send({message: 'Sender cannot be empty'});
	}
	if(!req.body.subject) {
		return res.status(400).send({message: 'Subject cannot be empty'});
	}
	if(!req.body.content) {
		return res.status(400).send({message: 'Content cannot be empty'});
	}
	
	var email = new Emails({
		sender: req.body.sender,
		subject: req.body.subject,
		content: req.body.content
	});
	
	email.save( (err, data) => {
		if(err) {
			console.log(err);
			res.status(500).send({message: 'some error occured white creating the email'});
		} else {
			res.send(data);
		}
	});
}

//update
exports.update = function(req, res) {
	/*let _movie_id = req.params.movieId;
	
	Movie.findById(_movie_id, (err, movie) => {
		if(err) {
			console.log(err);
			if(err.kind === 'ObjectId') {
				return res.status(400).send({message: 'Movie not found with id ' + _movie_id});
			}
			return res.status(500).send({message: 'Error retrieving movie with id ' + _movie_id});
		}
		
		if(!movie) {
			return res.status(404).send({message: 'Movie not found with id ' + _movie_id});
		}
		
		movie.title = req.body.title;
		
		movie.save((err, data) => {
			if(err) {
				res.status(500).send({message: 'Could not update movie with id ' + _movie_id});
			} else {
				res.send(data);
			}
		});
		
	});*/
}

//delete
exports.delete = function(req, res) {
	/*let _movie_id = req.params.movieId;
	
	Movie.findByIdAndRemove(_movie_id, (err, movie) => {
		if(err) {
			console.log(err);
            if(err.kind === 'ObjectId') {
                return res.status(404).send({message: "Movie not found with id " + _movie_id});                
            }
            return res.status(500).send({message: "Could not delete movie with id " + _movie_id});
		}
		
		if(!movie) {
			return res.status(404).send({message: 'Movie not found with id ' + _movie_id});
		}
		
		res.send({message: "Movie deleted successfully!"})
		
	});*/
}

//generate fake data
exports.createFakeData = (req, res) => {
	for (var i = 0; i < 10; i++) {
		var emails = new Emails({
			sender: faker.internet.email(),
			subject: faker.lorem.word(),
			content: faker.lorem.text()
		});
		emails.save( (err, data) => {
			if(err) {
				console.log(err);
				//res.status(500).send({message: 'some error occured white creating the emails'});
			} else {
				//res.send(data);
				console.log(data);
			}
		});
	}
	res.redirect('/');
}