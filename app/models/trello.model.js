let mongoose = require('mongoose');

let TrelloCardSchema = mongoose.Schema({
	title: String,
	board: String,
	list: String,
}, {
	timestamps: true
});

module.exports = mongoose.model('TrelloCard', TrelloCardSchema);