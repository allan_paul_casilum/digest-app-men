let mongoose = require('mongoose');

let EmailSchema = mongoose.Schema({
	sender: String,
	subject: String,
	content: String,
}, {
	timestamps: true
});

module.exports = mongoose.model('Email', EmailSchema);