module.exports = function(app) {
	let emails = require('../controllers/emails.controllers.js');
	let notes = require('../controllers/notes.controllers.js');
	let trello_cards = require('../controllers/trello-cards.controllers.js');
	
	//route for emails
	//retrieve all emails
	app.get('/emails', emails.index);
	app.post('/api-create-email', emails.apiCreate);
	app.get('/emails-generate-fake-data', emails.createFakeData);
	//route for emails
	
	//route for notes
	app.get('/notes', notes.index);
	app.get('/notes-generate-fake-data', notes.createFakeData);
	//route for notes
	
	//route for trello cards
	app.get('/trello-cards', trello_cards.index);
	app.get('/trello-cards-generate-fake-data', trello_cards.createFakeData);
	//route for trello cards
	
}