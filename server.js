const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const faker = require('faker');

//create express app
let app = express();
let _public_path = __dirname;

//db
let dbConfig = require('./config/database.config.js');
mongoose.Promise = global.Promise;
mongoose.connect(dbConfig.localhost);
mongoose.connection.on('error', () => {
	console.log('Could not connect to database. Exiting now.');
	process.exit();
});
mongoose.connection.once('open', () => {
	console.log('Successfully connected to the database');
});

app.set('view engine', 'ejs')

//parser request of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

//parse request of content-type - application/json
app.use(bodyParser.json());

//define a simple route
app.get('/', (req, res) => {
	res.render(_public_path + '/views/index.ejs', {title: 'Test'});
});

//Require routes
require('./app/routes/web.routes.js')(app);

//listen for request
app.listen(8080, () => {
	console.log('server is listening to 8080');
	console.log(_public_path);
});